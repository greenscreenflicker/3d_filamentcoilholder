$fn=100;
difference(){
    union(){
        cylinder (h=1.5,r=60/2);
        translate(v=[0,0,1.5]) cylinder (h=5,r=53/2);
    }
    union(){
        cylinder (h=25,r=12.7*0.5);
    }
}